#include <string>
#include <map>
#include <vector>
#include <random>
#include <iostream>

using namespace std;

#ifdef USE_PROFILER
#include <gperftools/profiler.h>
#endif

static constexpr long stepCount = 1'000'000;
static constexpr long keyLength = 32;
static constexpr long maxValue = 1'000;

static string generateKey(mt19937& engine)
{
    static uniform_int_distribution<char> charDist(0, 25);

    string key(keyLength, '\0');

    for (char& c : key)
    {
        c = 'a' + charDist(engine);
    }

    return key;
}

static void prepareData(vector<string>& keys, map<string, long>& values)
{
    static uniform_int_distribution<long> valueDist(0, maxValue);

    random_device device;
    mt19937 engine(device());

    for (long i = 0; i < stepCount; i++)
    {
        string key = generateKey(engine);

        long value = valueDist(engine);

        keys.push_back(key);
        values.insert({ key, value });
    }
}

static long computeResult(const vector<string>& keys, const map<string, long>& values)
{
    long result = 0;

    for (const string& key : keys)
    {
        result += values.at(key);
    }

    return result;
}

int main(int, char**)
{
#ifdef USE_PROFILER
    ProfilerStart("profile.log");
#endif

    vector<string> keys;
    map<string, long> values;

    prepareData(keys, values);

    long result = computeResult(keys, values);

    cout << "Computed result: " << result << endl;

#ifdef USE_PROFILER
    ProfilerStop();
#endif

    return 0;
}
