# C Profiling With gperftools

This is the example repository for [my tutorial](https://amini-allight.org/post/c-profiling-with-gperftools) on profiling C/C++ applications using gperftools.

The demo program computes a useless random value to reach the minimum CPU time requirement for gperftools (10 ms) and to provide something for the profiler to profile.

## Dependencies

To compile and profile the example program you will need:

- gperftools
- kcachegrind (to view the finished profile)

## Usage

To build the program with profiler enabled run:

```sh
./build.sh profile
```

To profile the application run:

```sh
./profile.sh
```

The program will run and then exit, after which `kcachegrind` will open to view the profile. To compile and run the program without profiling run:

```sh
./build.sh run
./demo
```

## License

Created by Amini Allight. The contents of this repository are licensed under Creative Commons Zero (CC0 1.0), placing them in the public domain.
