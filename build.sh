#!/bin/sh
set -eu
if [ $# -ne 1 ]; then
    echo "Usage: $0 profile|run"
    exit 1
fi
if [ "$1" = "profile" ]; then
g++ -o demo -DUSE_PROFILER -lprofiler demo.cpp
else
g++ -o demo demo.cpp
fi
