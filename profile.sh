#!/bin/sh
set -eu
./demo
pprof --callgrind ./demo profile.log > profile.callgrind
kcachegrind profile.callgrind
